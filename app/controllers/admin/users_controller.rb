class Admin::UsersController < AdminController

  def index
    # !!! Le callback du controlleur parent ("AdminControlleur") est applé avant d'arriver ici et avant les callback de la classe enfant!!!
    @joueurs = Joueur.all
    #byebug

    respond_to do |format|
      format.html { render :html => @joueurs.to_json } # Le render html transforme le json en texte html
      format.json { render :json => @joueurs.to_json }
      format.xml { render :xml => @joueurs.collect{|j| [id: j.id, fullname: j.fullname] } } # Forme contracté de formatage des données voir documentation du langage ruby
    end
  end

end
