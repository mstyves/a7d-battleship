# CHAQUE ACTION SUIVI D'UN NUMERO EST A TITRE PEDAGOGIQUE SEULEMENT (ex. index_1)
# En tout temps, la méthode "byebug" peut être ajouté afin de stoper une requête et vérifier de l'information ou faire des tests.
class ExempleController < ApplicationController
  # Il existe des callbacks à différents moments
  # https://edgeguides.rubyonrails.org/action_controller_overview.html#filters
  # https://api.rubyonrails.org/classes/AbstractController/Callbacks/ClassMethods.html
  before_action :get_parties, only: [:index_4, :index_5, :index_6] # L'inverse est possible, toute "except: [:index_1, :index_2]"
  layout false # ceci est pour éviter une confusion avec la partie d'explication sur les layouts voir exemple_template

  # Exemple démontrant l'intégration des modèles dans un controlleur
  def index_1
    # Recherche dans la base de données
    @parties = Partie.all

    # Modélisation du texte à retourner au navigateur
    @joueurs = @parties.collect do |p|
      if (p.joueur_2)
        "Joueur 1: " + p.joueur_1.fullname + " VS Joueur 2: " + p.joueur_2.fullname
      else
        "Joueur 1: " + p.joueur_1.fullname + " VS Joueur 2: Libre"
      end
    end

    # Texte retourné au navigateur (choisir une seul ligne "render")
    #render plain: @joueurs.to_s # Le texte est retourné tel quel sans traitement spécial
    render html: @joueurs.to_s # Le texte est adapté au html
  end

  # Exemple démontrant l'intégration des modèles dans un controlleur
  def index_2
    # Recherche dans la base de données
    @parties = Partie.all

    # Modélisation du texte à retourner au navigateur
    @joueurs = @parties.collect do |p|
      {joueur_1: p.joueur_1, joueur_2: p.joueur_2}
    end

    # Texte retourné au navigateur
    render json: @joueurs
  end

  # Exemple démontrant l'intégration des modèles dans un controlleur
  def index_3
    # Recherche dans la base de données
    @parties = Partie.all

    # Modélisation du texte à retourner au navigateur
    @joueurs = @parties.collect do |p|
      {joueur_1: p.joueur_1, joueur_2: p.joueur_2 }.as_json # Pour générer du xml, l'objet doit être sous forme d'un json
    end

    # Texte retourné au navigateur
    render xml: @joueurs
  end

  # Exemple utilisant les callbacks
  def index_4
    # Recherche dans la base de données !!!Voir le callback!!!


    # Modélisation du texte à retourner au navigateur
    @joueurs = @parties.collect do |p|
      if (p.joueur_2)
        "Joueur 1: " + p.joueur_1.fullname + " VS Joueur 2: " + p.joueur_2.fullname
      else
        "Joueur 1: " + p.joueur_1.fullname + " VS Joueur 2: Libre"
      end
    end

    # Texte retourné au navigateur
    render json: @joueurs
  end

  # Exemple utilisant les mimetypes (!!!Notez que cette action remplace toutes celles plus haut!!!)
  def index_5
    # Recherche dans la base de données !!!Voir le callback!!!


    # Modélisation du texte à retourner au navigateur
    @joueurs = @parties.collect do |p|
      {joueur_1: p.joueur_1, joueur_2: p.joueur_2 }.as_json # Pour générer du xml, l'objet doit être sous forme d'un json
    end

    # l'url répond au format demandé ex: /battleships5.json, /battleships5.xml...
    # Le traitement reste le même, seul le format de la réponse change
    respond_to do |format|
      format.html { render :html => @joueurs.to_s }
      format.json { render :json => @joueurs.to_json }
      format.xml { render :xml => @joueurs.to_xml }
    end
  end

  # Exemple utilisant les mimetypes (!!!Notez que cette action remplace toutes celles plus haut!!!)
  def index_6
    # Recherche dans la base de données !!!Voir le callback!!!


    # Modélisation du texte à retourner au navigateur
    # Partie non necessaire puisque le formatage est fait à la vue (views/battleship/index_6.html.erb)
    #@joueurs = @parties.collect do |p|
    #  {joueur_1: p.joueur_1, joueur_2: p.joueur_2 }.as_json # Pour générer du xml, l'objet doit être sous forme d'un json
    #end

    # l'url répond au format demandé ex: /battleships5.json, /battleships5.xml...
    # Le traitement reste le même, seul le format de la réponse change
    respond_to do |format|
      format.html { render 'exemple/index_6' }
      format.json # Par défaut, le framework va chercher le nom de la template suivant le nom de l'action
      format.xml  # Le répertoire de la vue est directement relié au nom du contrôleur
      #format.json { render 'battleship/index_7' } # Cet exemple utilise le jbuilder, un systeme de templating pour le json
    end
  end

  # Exemple utilisant un parametre
  # Notez bien que les paramêtres provenant de l'URL sont traités de la même façon que les paramêtre d'un formulaire
  # Via la méthode "params" qui retourne une table de hashage
  def show_1
    # Recherche dans la base de données
    @partie = Partie.find(params[:id])

    # Modélisation du texte à retourner au navigateur
    @joueurs = {joueur_1: @partie.joueur_1, joueur_2: @partie.joueur_2 }.as_json # Pour générer du xml, l'objet doit être sous forme d'un json

    # l'url répond au format demandé ex: /battleships5.json, /battleships5.xml...
    # Le traitement reste le même, seul le format de la réponse change
    respond_to do |format|
      format.html { render :html => @joueurs.to_s }
      format.json { render :json => @joueurs.to_json }
      format.xml { render :xml => @joueurs.to_xml }
    end
  end

  # Tout ce qui suis ce mot est déclaré comme private, par défaut tout est public (les actions plus haut)
  private

  # Methode utilisé dans plusieurs action du contrôleur
  def get_parties
    @parties = Partie.all
  end

end
