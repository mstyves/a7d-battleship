class CssController < ApplicationController
  # Sous-entendu du nom du layout (views/layouts/css.html.erb)
  #layout 'css'
  before_action :get_parties
  def index_1

  end

  # Tout ce qui suis ce mot est déclaré comme private, par défaut tout est public (les actions plus haut)
  private

  # Methode utilisé dans plusieurs action du contrôleur
  def get_parties
    @parties = Partie.all
  end
end