# CHAQUE ACTION SUIVI D'UN NUMERO EST A TITRE PEDAGOGIQUE SEULEMENT (ex. index_1)
# En tout temps, la méthode "byebug" peut être ajouté afin de stoper une requête et vérifier de l'information ou faire des tests.
class ExempleTemplateController < ApplicationController
  before_action :get_parties, except: [:index_5, :index_post_5, :index_6]

  #layout false

  # Exemple utilisant aucun layout
  def index_1

    respond_to do |format|
      format.html { render layout: false } # Voir le résultat dans le html généré sur chrome et comparer avec index_2
    end
  end


  # Exemple utilisant le layout par defaut (views/layouts/application.html.erb)
  def index_2

    # ces lignes sont sous-entendues
    #respond_to do |format|
    #  format.html
    #end
  end


  # Exemple utilisant un layout custom
  def index_3

    respond_to do |format|
      format.html { render layout: 'exemple1' }
    end
  end


  # Exemple utilisant un layout et une partiel en boucle
  # https://edgeguides.rubyonrails.org/layouts_and_rendering.html
  def index_4

  end

  # Exemple utilisant le un formulaire
  def index_5

  end

  # Exemple utilisant qui recois le formulaire
  def index_post_5
    @joueur = Joueur.new(nom: params[:nom], prenom: params[:prenom], email: params[:email])

    respond_to do |format|
      if @joueur.save # Si la sauvegarde se passe bien, on redirige vers l'action index_5 pour rafraichir le formulaire
        format.html {redirect_to '/exemple_template_5'}
      else
        format.html # Si une erreur arrive, on l'affiche sur le formulaire
      end
    end
  end

  # exemple en utilisant les modèles
  def index_6
    @joueur = Joueur.new
  end

  def index_post_6
    joueur_params = params.require(:joueur).permit(:nom, :prenom, :email) # Securite lors d'une assignation de masse, on filtre les paramètres authoriser avant de les envoyer au modèle qui gère la base de données
    @joueur = Joueur.new(joueur_params)

    respond_to do |format|

      if @joueur.save # Si la sauvegarde se passe bien, on redirige vers l'action index_5 pour rafraichir le formulaire
        format.html {redirect_to '/exemple_template_6'}
      else
        format.html {render 'index_6' } # Si une erreur arrive, on l'affiche sur le formulaire d'origine
      end
    end
  end

  # Exemple utilisant un css, et des assets
  def index_7

  end

  # Exemple utilisant un parametre
  # Notez bien que les paramêtres provenant de l'URL sont traités de la même façon que les paramêtre d'un formulaire
  # Via la méthode "params" qui retourne une table de hashage
  def show_1
    # Recherche dans la base de données
    @partie = Partie.find(params[:id])

    # Modélisation du texte à retourner au navigateur
    @joueurs = {joueur_1: @partie.joueur_1, joueur_2: @partie.joueur_2 }.as_json # Pour générer du xml, l'objet doit être sous forme d'un json

    # l'url répond au format demandé ex: /battleships5.json, /battleships5.xml...
    # Le traitement reste le même, seul le format de la réponse change
    respond_to do |format|
      format.html { render :html => @joueurs.to_s }
      format.json { render :json => @joueurs.to_json }
      format.xml { render :xml => @joueurs.to_xml }
    end
  end

  # Tout ce qui suis ce mot est déclaré comme private, par défaut tout est public (les actions plus haut)
  private

  # Methode utilisé dans plusieurs action du contrôleur
  def get_parties
    @parties = Partie.all
  end

end
