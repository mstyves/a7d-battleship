module ApplicationHelper

  def partie_status(partie)
    case partie.status
    when "In Progress"
      "success"
    when "Waiting"
      "warning"
    when "End"
      "info"
    else
      "error"
    end
  end
end
