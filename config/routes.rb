Rails.application.routes.draw do
  devise_for :users

  root 'accueil#presentation'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Exemple utilisant uniquement le controlleur (Classe: BattleshipsControlleur, méthode: index_1)
  get '/exemple1' , to: 'exemple#index_1'

  # Exemple utilisant uniquement le controlleur et du JSON
  get '/exemple2' , to: 'exemple#index_2'

  # Exemple utilisant uniquement le controlleur et du XML
  get '/exemple3' , to: 'exemple#index_3'

  # Exemple utilisant les callbacks
  get '/exemple4' , to: 'exemple#index_4'

  # Exemple utilisant les mimetypes
  get '/exemple5' , to: 'exemple#index_5'

  # Exemple utilisant les mimetypes
  get '/exemple6' , to: 'exemple#index_6'

  # Exemple utilisant un parametre nommé "id", le ":" represente un espace réservé à un paramètre
  get '/exemple/:id', to: 'exemple#show_1', as: 'exemple_show' # Lorsque la route est ambigue par rapport aux autres, on defini un nom unique avec le as

  get '/exemple_template_1' , to: 'exemple_template#index_1'

  # Exemple utilisant uniquement le controlleur et du JSON
  get '/exemple_template_2' , to: 'exemple_template#index_2'

  # Exemple utilisant uniquement le controlleur et du XML
  get '/exemple_template_3' , to: 'exemple_template#index_3'

  # Exemple utilisant les callbacks
  get '/exemple_template_4' , to: 'exemple_template#index_4'

  # Exemple utilisant les mimetypes
  get '/exemple_template_5' , to: 'exemple_template#index_5'

  post '/exemple_template_post_5' , to: 'exemple_template#index_post_5'

  # Exemple utilisant les mimetypes
  get '/exemple_template_6' , to: 'exemple_template#index_6'
  post '/exemple_template_post_6' , to: 'exemple_template#index_post_6'

  # Exemple utilisant uniquement le controlleur (Classe: BattleshipsControlleur, méthode: index_1)
  get '/css1' , to: 'css#index_1'

  # Exemple utilisant uniquement le controlleur et du JSON
  get '/css2' , to: 'css#index_2'

  # Exemple utilisant uniquement le controlleur et du JSON
  get '/restricted' , to: 'restricted#index'

  namespace :admin do
    get '/users/:is_admin', to: 'users#index'
    get '/users', to: 'users#index'
  end
end
