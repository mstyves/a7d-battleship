# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_10_133145) do

  create_table "joueurs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email"
    t.string "password"
    t.string "nom"
    t.string "prenom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mouvements", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "joueur_id"
    t.bigint "partie_id"
    t.string "lettre"
    t.integer "numero"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["joueur_id"], name: "index_mouvements_on_joueur_id"
    t.index ["partie_id"], name: "index_mouvements_on_partie_id"
  end

  create_table "parties", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "joueur_1_id"
    t.bigint "joueur_2_id"
    t.bigint "gagnant_id"
    t.text "plateau_joueur_1", null: false
    t.text "plateau_joueur_2", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gagnant_id"], name: "index_parties_on_gagnant_id"
    t.index ["joueur_1_id"], name: "index_parties_on_joueur_1_id"
    t.index ["joueur_2_id"], name: "index_parties_on_joueur_2_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "mouvements", "joueurs"
  add_foreign_key "mouvements", "parties"
  add_foreign_key "parties", "joueurs", column: "gagnant_id"
  add_foreign_key "parties", "joueurs", column: "joueur_1_id"
  add_foreign_key "parties", "joueurs", column: "joueur_2_id"
end
